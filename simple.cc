#include <legion.h>

#include <chrono>
#include <iostream>
#include <random>

using namespace Legion;

/**
 * C = number of cycles per task
 * N = number of tasks
 */
const size_t C = 500, N = 4;

enum TaskIDs {
  TID_TOP_LEVEL,
  TID_CYCLE,
  TID_ADD
};

void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, Runtime *runtime)
{
  auto start = std::chrono::steady_clock::now();

  Future vals[N];
  uint_fast64_t seeds[N];

  // Compute values
  for (size_t i = 0; i < N; ++i)
  {
    seeds[i] = static_cast<uint_fast64_t>(i);
    TaskLauncher launcher(TID_CYCLE, TaskArgument(&seeds[i],
          sizeof(uint_fast64_t)));
    vals[i] = runtime->execute_task(ctx, launcher);
  }

  // Add results
  Future sum;
  {
    TaskLauncher launcher(TID_ADD, TaskArgument(NULL, 0));
    for (size_t i = 0; i < N; i++) {
      launcher.add_future(vals[i]);
    }
    sum = runtime->execute_task(ctx, launcher);
  }

  double dsum = sum.get_result<double>();
  std::cout << "Sum: " << dsum << std::endl;
  std::cout << "Time: " << std::chrono::duration<double,std::milli>(std::chrono::steady_clock::now()
        - start).count() << " ms" << std::endl;
}

double cycle_task(const Task *task,
                  const std::vector<PhysicalRegion> &regions,
                  Context ctx, Runtime *runtime)
{
  assert(task->arglen == sizeof(uint_fast64_t));
  uint_fast64_t seed = *(const uint_fast64_t *)task->args;

  std::mt19937_64 eng(seed);
  std::uniform_real_distribution<double> dist(0.0, 1.0);
  for (size_t i = 0; i < C; ++i) {
    dist(eng);
  }

  return dist(eng);
}

double add_task(const Task *task,
                const std::vector<PhysicalRegion> &regions,
                Context ctx, Runtime *runtime)
{
  assert(task->futures.size() == N);
  double sum = 0.0;
  for (size_t i = 0; i < N; ++i) {
    sum += task->futures[0].get_result<double>();
  }
  return sum;
}

int main(int argc, char **argv)
{
  Runtime::set_top_level_task_id(TID_TOP_LEVEL);

  {
    TaskVariantRegistrar registrar(TID_TOP_LEVEL, "top_level");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    Runtime::preregister_task_variant<top_level_task>(registrar, "top_level");
  }

  {
    TaskVariantRegistrar registrar(TID_CYCLE, "cycle");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<double, cycle_task>(registrar, "init_mtx");
  }

  {
    TaskVariantRegistrar registrar(TID_ADD, "add");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.set_leaf();
    Runtime::preregister_task_variant<double, add_task>(registrar, "init_mtx");
  }

  return Runtime::start(argc, argv);
}
